#ifndef JSON_H
#define JSON_H
#ifdef __cplusplus
#include <cstdlib>
#include <cstdio>
#include <cstring>
extern "C" {

#else

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#endif

typedef enum {
	TOKEN_STRING,
	TOKEN_STRING_ARRAY,
	TOKEN_NUMBER,
	TOKEN_JSON,
	TOKEN_JSON_ARRAY
} TOKEN_TYPE;

typedef struct _json *JSON;

typedef struct {
	TOKEN_TYPE type;
	char* name;
	union {
		char* string;
		struct {
			size_t arraySize;
			char** stringArray;
		};
		double number;
		struct {
			JSON* jsonArray;
			size_t jsonCount;
		};
		JSON json;

	};
} *TOKEN;

struct _json{
	size_t count;
	TOKEN* tokens;
} ;

JSON parsingJson(char* value);
TOKEN getToken(JSON, char*);
char* getString(char* );
void releaseJson(JSON );
void addToken(JSON, TOKEN);
void deleteToken(JSON, TOKEN);
char* jsonToString(JSON);
void fprintToken(FILE*, TOKEN);
void fprintJson(FILE*, JSON);
#ifdef __cplusplus
}
#endif
#endif