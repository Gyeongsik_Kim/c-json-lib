#include "json.h"
extern JSON parsingJson(char* value) {
	if (strtok(value, "{[") != NULL & strtok(value, "}]") != NULL) {
		JSON json = (JSON)malloc(sizeof(JSON));
		json->tokens = NULL;
		json->count = 0;
		int level = 0, isName = 0;
		char* name;
		void* temp;
		while (value++) {
			switch ((int)value) {
				case '{': level++; break;
				case '}':level--; break;
				case '"': isName = 1; name = getString(value); break;
				case ':': isName = 0; break;
				default: {
					
					break;
				}
			}
		}
		return json;
	}
	return NULL;
}

static char* getString(char* value) {
	char* temp = value;
	while (value++);
	char* result = (char*)malloc(sizeof(char) * (abs(value - temp) + 1));
	memcpy(result, temp, abs(value - temp) + 1);
	return result;
}

extern void releaseJson(JSON json) {
	for (size_t i = 0; i < json->count; i++) {
		TOKEN temp = json->tokens[i];
		free(temp->name);
		switch (temp->type) {
			case TOKEN_STRING:
				free(temp->string);
				break;
			case TOKEN_STRING_ARRAY:
				for (size_t in = 0; in < temp->arraySize; i++)
					free(temp->stringArray[in]);
				break;
			case TOKEN_NUMBER:
				break;
			case TOKEN_JSON:
				releaseJson(temp->json);
				break;
		}
		free(json->tokens[i]);
	}
	json->count = 0;
}

extern void deleteToken(JSON json, TOKEN token)
{
	TOKEN deleteTemp;
	size_t deleteIndex = -1;
	for (size_t i = 0; i < json->count; i++) {
		if (memcmp(json->tokens[i], token, sizeof(TOKEN)) == 0) {
			deleteIndex = i;
			break;
		}
	}
	if (deleteIndex != -1) {
		size_t backup = json->count;
		TOKEN* temp = (TOKEN*)malloc(sizeof(TOKEN) * json->count);
		memcpy(temp, json->tokens, json->count * sizeof(TOKEN));
		releaseJson(json);
		for (size_t i = 0; i < backup; i++) {
			if (i != deleteIndex)
				addToken(json, temp[i]);
		}
		free(temp);
	}
}

extern void addToken(JSON json, TOKEN token)
{
	if (json->count == 0) {
		json->count = 1;
		json->tokens = (TOKEN)malloc(sizeof(TOKEN));
		memcpy(json->tokens, token, sizeof(TOKEN));
		free(token);
	}
	else {
		json->count += 1;
		realloc(json->tokens, sizeof(TOKEN) * json->count);
		memcpy(json->tokens[json->count - 1], token, sizeof(TOKEN));
		free(token);
	}
}

extern void fprintToken(FILE* fd, TOKEN token) {
	switch (token->type) {
	case TOKEN_STRING:
		fprintf(fd, "%s : %s\n", token->name, token->string);
		break;
	case TOKEN_NUMBER:
		fprintf(fd, "%s : %d\n", token->name, token->number);
		break;
	case TOKEN_STRING_ARRAY:
		fprintf(fd, "%s : [", token->name);
		for (size_t i = 0; i < token->arraySize; i++) {
			fprintf(fd, "%s, ", token->stringArray[i]);
		}
		fprintf(fd, "]\n");
		break;
	case TOKEN_JSON:
		fprintJson(fd, token->json);
		break;
	}
}

extern void fprintJson(FILE* fd, JSON json) {
	fprintf(fd, "{\n");
	for (size_t i = 0; i < json->count; i++)
		fprintToken(fd, json->tokens[i]);
	fprintf(fd, "}\n");
}
extern TOKEN getToken(JSON json, char* key)
{
	if (json->count <= 0)
		return NULL;
	for (size_t i = 0; i < json->count; i++) {
		TOKEN temp = json->tokens[i];
		if (strncmp(temp->name, key, strlen(key) - 1) == 0) {
			return (json->tokens[i]);
		}
	}
	return NULL;
}
